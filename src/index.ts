var app = require('express')();

const strategies = require('./strategy/strategy');
// var areaCalc = require('./singleton');
// var HouseBuilder = require('./Builder/house-builder');
// import strategies from "./strategy/strategy";
import { Singleton } from './singleton';
import { HouseBuilder } from './builder/builder';

var port = process.env.PORT || 3000;

// app.get('/', function() {
// 	res.send('<html><head></head><body><h1>Hello world!</h1></body></html>');
// });

app.listen(port, ()=> {
    console.log(`listening on port:${port}`);
});

app.get('/area', function() {
    console.log(new Singleton().circle(2));
});

app.get('/strategy', function() {
    console.log(strategies);
});

app.get('/builder', function() {
    const myHouse = new HouseBuilder('John Street 14')
                    .setFloor(4)
                    .makeParking()
                    .makeGarden()
                    .build();
     console.log(myHouse);
});
