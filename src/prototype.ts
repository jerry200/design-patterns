export class Person {
    firstName: string
    lastName: string
    constructor(fname: string, lname: string) {
        this.firstName = fname
        this.lastName = lname
    }
}

export class Speak extends Person {
    greet = () => {};
    constructor(fname: string, lname: string) {
        super(fname, lname);
        this.greet = () => {
            var message = 'Greetings from ' + this.firstName + ' ' + this.lastName;
            console.log(message);
            return message
        }
    }
} 