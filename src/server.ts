import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import moment = require('moment');

import { Singleton } from './singleton';
import { HouseBuilder } from './builder/builder';
import { Request, Response } from 'express-serve-static-core';
import  EmpFactory  from './factory/factory';
import empNoFactory from './factory/no-factory';
import StrategyLogger from './strategy/strategy';
import { Speak } from './prototype';

class Server {
    public expressApp = express();
    public router = express.Router();

    constructor() {

        this.config();
        this.routes();
    }

    public config(): void {

        // express middleware
        this.expressApp.use(bodyParser.urlencoded({ extended: true, limit: '15mb' }));
        this.expressApp.use(bodyParser.json({ limit: '50mb' }));
    
        this.expressApp.use(compression());
        this.expressApp.use(helmet());
    
        this.expressApp.use((request, response, next) => {
          response.header('Access-Control-Allow-Origin', '*');
          response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
          next();
        });
    }

    public routes(): void {
        this.expressApp.get('/' , (req : Request , res : Response) => {
            return res.status(200).json({
              okay: 'true'
            });
          });

        this.expressApp.get('/status', (req : Request , res : Response) => {
        return res.status(200).json({
            version: process.env.VERSION,
            deployed: moment().format()
        });
        })

        this.expressApp.post('/calarea', function(req : Request , res : Response) {
            var area = new Singleton().circle(req.body.radius);
            console.log(area);
            return res.status(200).json({
                result: area
            })
        });

        this.expressApp.get('/builder', function(req : Request , res : Response) {
            const myHouse = new HouseBuilder('John Street 14')
                            .setFloor(4)
                            .makeParking()
                            .makeGarden()
                            .build();
             console.log(myHouse);
        });

        this.expressApp.post('/factory', function(req : Request , res : Response) {
            var employee = new EmpFactory().create(req.body.fName, req.body.lName, req.body.type);
            return res.status(200).json(employee)
        });

        this.expressApp.post('/nofactory/engineer', function(req: Request, res: Response) {
            var engineer = new empNoFactory().engineer(req.body.fName, req.body.lName)
            return res.status(200).json( engineer ) 
        });

        this.expressApp.post('/nofactory/manager', function(req: Request, res: Response) {
            var manager = new empNoFactory().manager(req.body.fName, req.body.lName)
            return res.status(200).json( manager )
        });

        this.expressApp.post('/nofactory/director', function(req: Request, res: Response) {
            var director = new empNoFactory().director(req.body.fName, req.body.lName)
            return res.status(200).json( director )
        });

        this.expressApp.get('/strategy', function(req: Request, res: Response) {
            new StrategyLogger().logStrategies();
            return res.status(200).json({
                'status': 'ok',
                'message': 'Successfully logged all Strategies'
            })
        });

        this.expressApp.post('/prototype',  function(req: Request, res: Response) {
            var greeetings = new Speak(req.body.fname, req.body.lname).greet();
            return res.status(200).json({
                status: 'ok',
                message: greeetings
            })
        })
    }
}

export default new Server().expressApp;