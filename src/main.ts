var debug = require('debug');
var http = require('http');

import expressApp from './server';

const app_port = process.env.APP_PORT || 3000;
let server: any = {};

   // Initializing dependencies
    debug('ts-express:server');

    server = http.createServer(expressApp);

    server.listen(app_port, function () {
        const addressInfo = server.address();
        console.log('Server listening on port: ' + addressInfo["port"]);
        server.on('error', onError);
        server.on('listening', onListening);
    }); 
   
    
/**
 * onError
 *
 * @param {NodeJS.ErrnoException} error
 */
function onError(error: NodeJS.ErrnoException): void {
    if (error.syscall !== 'listen') {
        throw error;
    }
    const bind = (typeof app_port === 'string') ? 'Pipe ' + app_port : 'Port ' + app_port;
    switch (error.code) {
        case 'EACCES':
            console.log(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.log(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * onListening
 *
 */
function onListening(): void {
    const addr = server.address();
    const bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}
   
    