//area.js
export class Singleton {

  public circle(radius: number) {
    return radius * radius * Math.PI;
  }

}

// let singleton = new Singleton();
// module.exports = singleton;
// module.exports = Singleton;
