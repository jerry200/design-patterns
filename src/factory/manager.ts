export default class Manager{
    firstName: string;
    lastName: string;
    designation: string;


    constructor(fname: string, lname: string){
        this.firstName = fname
        this.lastName = lname
        this.designation = 'Manager';
    }

    getName(){
        return this.firstName + " " + this.lastName
    }
}