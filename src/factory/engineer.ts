export default class Engineer{
    firstName: string;
    lastName: string;
    designation: string;

    constructor(fName: string, lName: string){
        this.firstName = fName
        this.lastName = lName
        this.designation = 'Engineer';
    }

    getName(){
        return this.firstName + " " + this.lastName
    }
}
