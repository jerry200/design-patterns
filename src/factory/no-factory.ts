import Engineer from './engineer';
import Manager from './manager';
import Director from './director';

export default class empNoFactory {
    engineer(fName: string, lName: string) {
        let engineer = new Engineer(fName, lName);
        console.log('Engineer is ', engineer.getName())
        return engineer
    }
    
    manager(fName: string, lName: string) {
        let manager = new Manager(fName, lName);
        console.log('Manager is ', manager.getName())
        return manager
    }
    
    director(fName: string, lName: string) {
        let director = new Director(fName, lName);
        console.log('Director is ', director.getName())
        return director
    }
}
