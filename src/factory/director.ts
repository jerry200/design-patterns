export default class Director {
    firstName: string;
    lastName: string;
    designation: string;

    constructor(fname: string, lname: string){
        this.firstName = fname
        this.lastName = lname
        this.designation = 'Director';
    }

    getName(){
        return this.firstName + " " + this.lastName
    }
}