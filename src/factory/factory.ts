import Engineer from './engineer';
import Manager from './manager';
import Director from './director';

export default class EmpFactory {
    
    create(fname: string, lname: string, type: string){
        if(type === 'engineer'){
            return new Engineer(fname,lname)
        } else if(type === 'manager') {
            return new Manager(fname, lname)
        } else if(type === 'director'){
            return new Director(fname, lname)
        }
    }
}
 