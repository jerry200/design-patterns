import TestStrategy from './test-strategy';
import StrategyOne from './strategy-one';
import StrategyTwo from './strategy-two';
import DefaultStrategy from './default-strategy';

export default class StrategyLogger {
    logStrategies() {
        new TestStrategy().getTestStrategy();
        new StrategyOne().getStrategyOne();
        new StrategyTwo().getStrategyTwo();
        new DefaultStrategy().getDefaultStrategy();
    }
}